$clients = [
  {
    'name' => 'Pablo',
    'company' => 'Google',
    'email' => 'pablo@google.com',
    'position' => 'Software engineer'
  },
  {
    'name' => 'Ricardo',
    'company' => 'Facebook',
    'email' => 'ricardo@facebook.com',
    'position' => 'Data engineer'
  }
]

def create_client(client)
  unless $clients.include? client
    $clients.push(client)
  else
    puts 'Client is already in the client\'s list'
  end
end


def list_clients
  $clients.each_with_index do |client, index|
    # puts "#{index}: #{client[:name]}"
    name = client["name"]
    company = client["company"]
    email = client["email"]
    position = client["position"]
    puts "#{index} | #{name} | #{company} | #{email} | #{position}"
  end
end

def update_client(client_id,updated_client)
  if client_id < $clients.length
    $clients[client_id] = updated_client
  else
    puts "Client is not in clients list"
  end
end

def delete_client(client_id)
  if client_id < $clients.length
    $clients.delete_at(client_id)
  else
    puts "Client is not in clients list"
  end
end

def search_client(client_name)
  $clients.each do |client|
    if client["name"].downcase == client_name.downcase
      return true
    end
  end
  false
end

#private
def print_welcome
  puts 'Welcome to Ruby Sales'
  puts '*' * 50
  puts 'What would you like to do today?'
  puts '[C]reate client'
  puts '[R]ead clients'
  puts '[U]pdate client'
  puts '[D]elete client'
  puts '[S]earch client'
end

def prompt(*args)
  print(*args)
  gets.chomp
end

#private
def get_client_name
  client_name = ""
  while client_name.empty?
    client_name = prompt 'What is the client name? '
    if client_name == 'exit'
      client_name = ""
      break
    end
  end
  exit if client_name.empty?
  
  client_name
end


def get_client_field(field_name)
  field = ""
  while field.empty?
    field = prompt "What is the client #{field_name}? "
  end
  field
end

def get_client
  client = {
    'name' => get_client_field('name'),
    'company' => get_client_field('company'),
    'email' => get_client_field('email'),
    'position' => get_client_field('position')
  }
end

def update_client_field(client, field_name)
  old_field = client[field_name]
  field = prompt "What is the client #{field_name}? (#{old_field}) "
  if field.empty?
    return old_field
  end
  field
end


def update_client_by_id(client_id)
  
  if client_id < $clients.length
    old_client = $clients[client_id]
  
    client = {
      'name' => update_client_field(old_client, 'name'),
      'company' => update_client_field(old_client, 'company'),
      'email' => update_client_field(old_client, 'email'),
      'position' => update_client_field(old_client, 'position')
    }
  else
    puts "Client is not in clients list"
    exit
  end
end


print_welcome
command = gets.chomp
command.upcase!
if command == 'R'
  list_clients
elsif command == 'C'
  client = get_client
  create_client(client)
  list_clients
elsif command == 'D'
  client_id = get_client_field('id').to_i
  # client_name = get_client_name
  delete_client(client_id)
  list_clients
elsif command == 'U'
  client_id = get_client_field('id').to_i
  updated_client = update_client_by_id(client_id)
  update_client(client_id, updated_client)
  list_clients
elsif command == 'S'
  client_name = get_client_name
  found = search_client(client_name)
  if found
    puts 'The client is in the client\'s list'
  else
    puts "The client: #{client_name} is not in our client's list"
  end
else
  puts 'Invalid Command'
end
